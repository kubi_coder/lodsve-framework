package lodsve.core.script.python;

import lodsve.core.script.AbstractScriptEngine;

/**
 * python.
 *
 * @author sunhao(sunhao.java@gmail.com)
 * @version 1.0 2016/12/9 上午11:20
 */
public class PythonScriptEngine extends AbstractScriptEngine {
    @Override
    protected String getScriptName() {
        return "python";
    }
}
