package lodsve.core.script.ruby;

import lodsve.core.script.AbstractScriptEngine;

/**
 * ruby.
 *
 * @author sunhao(sunhao.java@gmail.com)
 * @version 1.0 2016/12/9 上午11:21
 */
public class RubyScriptEngine extends AbstractScriptEngine {
    @Override
    protected String getScriptName() {
        return "ruby";
    }
}
