package lodsve.core.script.groovy;

import lodsve.core.script.AbstractScriptEngine;

/**
 * groovy.
 *
 * @author sunhao(sunhao.java@gmail.com)
 * @version 1.0 2016/12/9 上午11:16
 */
public class GroovyScriptEngine extends AbstractScriptEngine {
    @Override
    protected String getScriptName() {
        return "groovy";
    }
}
