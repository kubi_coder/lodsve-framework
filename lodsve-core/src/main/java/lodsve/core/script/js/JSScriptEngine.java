package lodsve.core.script.js;

import lodsve.core.script.AbstractScriptEngine;

/**
 * javascript.
 *
 * @author sunhao(sunhao.java@gmail.com)
 * @version 1.0 2016/12/9 上午11:17
 */
public class JSScriptEngine extends AbstractScriptEngine {
    @Override
    protected String getScriptName() {
        return "javascript";
    }
}
